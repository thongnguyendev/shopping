<?php
require_once 'controllers/Shopping_cartController.php';
$c = new Shopping_cartController;
// thêm sản phẩm vào giỏ hàng
if( isset($_POST['id']) && 
    isset($_POST['action']) && 
    $_POST['action']=='add' 
){
    $id = $_POST['id'];
    return $c->addToCart($id);
}
// xoá sản phảm vào giỏ hàng
if(isset($_POST['id']) && isset($_POST['action']) && $_POST['action']=='delete' ){
    //delete cart
    return $c->deleteCart($_POST['id']);
}
// update trong giỏ hàng
if(isset($_POST['id']) && isset($_POST['action']) && $_POST['action']=='update' ){
    //update cart
    return $c->updateCart($_POST['id'], $_POST['qty']);
}
?>