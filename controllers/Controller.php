<?php


// tên class chung vs file nhen 
require_once'models/ProductTypeModel.php';
require_once'helpers/Cart.php';
session_start();


class Controller{  // hàm thêm giờ viet nam, set time

///  php thuần làm theo hướng chức năng..  
    function __construct()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
    function loadView($view = "index",string $title='Home',  $data=[])
    {
        // tat ca doan code trrongfile master vao dong duoi nay nef  . dau .. la ra khoi thu muc controller, vi the phai ../views - thu muc chua file nay nef
if(isset($_SESSION['cart'])){
    $oldCart=$_SESSION['cart'];
    $cart=new Cart($oldCart);
    $totalItemCart=$cart->totalQty;

}
else $totalItemCart=0;

        $model=new ProductTypeModel();
        $categories=$model->getAllCategories();

        // viet truot dong requice
        require_once "views/master.view.php";
    }
    function loadViewAjax($view, $data = [])
    {
        require_once "views/ajax/$view.view.php";
    }
}

// chú ý
/**
 * create route
 * code controller
 * call view
 */
?>