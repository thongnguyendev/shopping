<?php
// ke thua lop controller

// tao tung controller cho tung trang, tranh bi loi ca dam

//goi lai file Controller.php
require_once 'Controller.php';
require_once 'models/ProductModel.php';

class ProductController extends Controller
{
    function loadProduct()
    {
        // kieemr tra neu load view ok 
        if (isset($_GET['url']) && $_GET['id']) {
            $url = $_GET['url']; //iphone-x-64gb-gray
            $id = $_GET['id']; // 105
            $model = new ProductModel;
            $product = $model->getProductDetail($url, $id); // chi tiêt san pham

            if ($product) {
                //load view ok thi 
                $relatedProducts = $model->getRelateProduct($product->id_type, $product->id);
                $title = $product->name;

                $data = [
                    'product' => $product,
                    'relatedProducts' => $relatedProducts

                ];
                return $this->loadView('product', $title, $data);
            } else {
                header('Location: error.php');
            }
        } else {
            // loi thi tro ve trang bao loi
            header('Location: error.php');
        }
    }
}