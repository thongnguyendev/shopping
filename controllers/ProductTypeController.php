<?php
require_once 'Controller.php';
// co the bo goi cung dc... sd cu phap require_onr oon honw
require_once 'models/ProductTypeModel.php';
require_once 'helpers/pager.php';

class ProductTypeController extends Controller{
    function loadProductType(){
      if(!isset($_GET['url'])){
            header('Location:home'); // home ~ index.php
            return;
        }
        $model = new ProductTypeModel;
        $url = $_GET['url'];
        $type = $model->getProductTypeByUrl( $url);
        if($type){
              // kieemr tra neu load view ok 
            $title = $type->name;
            $categoriesSmall = $model->getCategoriesWithOroductCount();


            ////// sd kỹ thuật phân trang
            
            // $page = 1;----> page mặc định là bằng 1
            // if(isset($_GET['page']))  ---> nếu mà page nó k rỗng thì
           // {
            //     $page = $_GET['page'];---> page current(hiện tại)-- chuyển về kiểu số nguyên
            // }
            $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;   // đây là cách tính theo ngôi thứ 3

///  có 1 cách tính khác dễ hình dung hơn... code ngay dưới đây

// if(!isset($_GET['page']) || $_GET['page']==0){
//             $page = 1;
//         }
//         else{
//             $page = (int)$_GET['page'];
//         }


            $quantity = 9;  // số lượng
            $position = ($page-1)*$quantity;      // chức vụ để tìm số trang mong muốn
            $productByType = $model->getProductByType($type->id,$position,$quantity);
            $tongSP= ($model->countProductByType($type->id))->tongSP;
            $soTrangHienThi=5;
            $pager= new Pager($tongSP,$page,$quantity,$soTrangHienThi);
            $paginateHtml=$pager->showPagination();
             //print_r($productByType);die;
            
            $data = [
                'categoriesSmall'=>$categoriesSmall,
                'productByType'=>$productByType,
                'title'=>$title,
                'paginateHtml' => $paginateHtml,
            ];
            return $this->loadView('product_type',$title,$data);
        }
        else{
            // loi thi tro ve trang bao loi
            header('Location: error.php');
            return;
        
        }
      }
    function postTypeAjax()
    {
        $id = $_POST['id'];
        $model = new ProductTypeModel();
        $products = $model->getProductByType($id, -1, -1);
        return $this->loadViewAjax('product-item', $products);
        // print_r(count($products));
    }
    }


/**
 * tongsp      = 350
 * sp/trang    = 10
 * => tong so trang = ceil(35/10) = 35 --> hàm làm tròn ceil
 * 
 * tranghientai      = 14 = x
 * sotranghienthi    = 11 = y
 * 
 * 
 * trang dau tien       = 9 = 14 - (11-1)/2 = x - (y-1)/2
 * trang cuoi           = 19 = x + (y-1)/2 = 9 + 11-1
 */
// 14 = (9+19)/2
// 14 = (9+9+11-1)/2
// => 14.2 = 2.9 + (11-1)
// => 9 = (14.2-(11-1))/2 = 14 - (11-1)/2
// 19 = 14 + (11-1)/2