<?php
// ke thua lop controller

// tao tung controller cho tung trang, tranh bi loi ca dam

//goi lai file Controller.php
require_once 'Controller.php';
require_once 'helpers/Cart.php';
require_once 'models/ShoppingCartModel.php';
require_once 'models/CheckoutModel.php';
require_once 'helpers/Helpers.php';
require_once 'helpers/PHPMailer/mailer.php';
//session_start();
class Shopping_cartController extends Controller
{

    function getHomePage(){
    $oldCart=isset($_SESSION['cart'])?$_SESSION['cart']:null;
    $cart=new Cart($oldCart);
    $data=['cart'=>$cart];
    return $this->loadView('shoppingcart','Giỏ hàng',$data);  
   }
// shoppingcart co nghia vd nhu la name trong shoppingcart.view.php 

    // viết 1 function nữa cho checkout ,, bỡi vì nó có cùng giao diện vs shoppingcart

    function getCheckoutView(){
        return parent::loadView('checkout');
    }
    // hamf them sp vao gio hang
    function addToCart($id){
        $model = new ShoppingCartModel;
        $product = $model->findProductById($id);
        if (!$product) {
            $r = [
                'error' => 1,
                'data' => null,
                'message' => 'Cannot find product!'
            ];
            echo json_encode($r);
            return false;
        }
        //add to cart
        $oldCart = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;
        $cart = new Cart($oldCart);
        $qty = isset($_POST['qty']) ? (int)$_POST['qty'] : 1;
        $cart->add($product, 1);
        $_SESSION['cart'] = $cart;
        $r = [
            'error' => 0,
            'data'=> [
                'cart'=>$cart,
                'product_name'=>$product->name,
                'images' => $product->image,
            ],
            'message' => 'Add to cart successfully!'
        ];
        echo json_encode($r);
        return true;
    }
    // hamf xoa sp khoi gio hang
    function deleteCart($id){
        if (isset($_SESSION['cart']) && $_SESSION['cart']->totalQty > 0) {
            $oldCart = $_SESSION['cart'];
            //kiem tra id co exist trong cart
            if (array_key_exists($id, $oldCart->items)) {
                $cart = new Cart($oldCart);
                $cart->removeItem($id);
                $_SESSION['cart'] = $cart;
                $r = [
                    'error' => 0,
                    'data' => [
                        'totalPrice' => number_format($cart->totalPrice),
                        'promtPrice' => number_format($cart->promtPrice),
                        'sellOff' => number_format($cart->promtPrice - $cart->totalPrice),
                        'cart' => $cart
                    ],
                    'message' => 'Deleted!'
                ];
                echo json_encode($r);
            } else {
                $r = [
                    'error' => 1,
                    'data' => null,
                    'message' => 'Cannot find id product'
                ];
                echo json_encode($r);
            }
        } else {
            $r = [
                'error' => 1,
                'data' => null,
                'message' => 'Cannot delete (Cart is empty)'
            ];
            echo json_encode($r);
        }
}
// ham update sp trong gio hang
    function updateCart($id, $qty){
        $model = new ShoppingCartModel;
        $product = $model->findProductById($id);
        if(!$product){
            $r = [
                'error'=> 1,
                'data'=> null,
                'message'=>'Cannot find product!'
            ];
            echo json_encode($r);
            return false;
        }
        $oldCart = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;
        $cart = new Cart($oldCart);
        $cart->update($product, $qty);
        $_SESSION['cart'] = $cart;
        $r = [
            'error'=> 0,
            'data'=> [
                'itemUpdate'=>number_format($cart->items[$id]['promotionPrice']),
                'totalPrice'=> number_format($cart->totalPrice),
                'promtPrice'=> number_format($cart->promtPrice),
                'sellOff'=> number_format($cart->promtPrice-$cart->totalPrice),
                'cart'=>$cart
            ],
            'message'=>'Success!'
        ];
        echo json_encode($r);
    }
    // hàm checkout 
    // kieemr tra cs lloix khi nhap sai hoac k cos gias tri

    function postCheckout(){
        // check all input must have value
        // validators input
        $name = $_POST['txtName'];
        $email = $_POST['txtEmail'];
        $gender = $_POST['gender'];
        $address = $_POST['txtAddress'];
        $phone = $_POST['txtPhone'];
        $paymentMethod = $_POST['payment_method'];
        $note = $_POST['txtNote'];
        $model = new CheckoutModel();
        $idCustomer = $model->insertCustomer($name, $gender, $email, $phone, $address);
        if(!$idCustomer) {
            $_SESSION['error_checkout'] = "Vui lòng thử lại nhé!";
            header('Location: thanh-toan.html');
        }
        
        else{
            $cart = $_SESSION['cart'];
            $total = $cart->totalPrice; 
            $promtPrice = $cart->promtPrice;
            $dateOrder = date('Y-m-d',time());
            
            $token = Helpers::createTokenString();
            $tokenDate = date('Y-m-d H:i:s',time());
            $idBill = $model->insertBill($idCustomer, $dateOrder, $total, $promtPrice, $paymentMethod, $note, $token, $tokenDate);
            if(!$idBill ) {
                $_SESSION['error_checkout'] = "Giỏ hàng của bạn hiện đang rỗng...Mời bạn mua sản phẩm trước khi thanh toán nha!";
                header('Location: thanh-toan.html');
            }
            else{
                foreach($cart->items 
                as $idProduct => $product){
                    // insert bill detail
                    $quantity = $product['qty'];
                    $price = $product['price'];
                    $discountPrice = $product['promotionPrice'];
                    $model->insertBillDetail($idBill, $idProduct, $quantity, $price, $discountPrice);
                    
                }
                
             //   gui mail xac nhan don hang
             if($gender=='Nam'){
                 $link="http://localhost:8000/shopping/oder/$token";
                  $contentMail = "<p>Dear anh $name,</p>
                <p>Cảm ơn chị đã đặt sản phẩm có mã là $idProduct và đơn giá $total VNĐ  với số lượng là  $quantity trên hệ thống của Thông-Shop PT2000 vào ngày $dateOrder  .</p>
                <p>Vui lòng nhấp vào <a href='$token'>liên kết sau</a> để xác nhận đơn hàng. </p>
                <p>Thank you!</p>";
             }
           else{
                 $contentMail = "<p>Dear chị $name,</p>
                <p>Cảm ơn bạn đã đặt sản phẩm có mã là $idProduct và đơn giá $total VNĐ  với số lượng là  $quantity trên hệ thống của Thông-Shop PT2000 vào ngày $dateOrder  .</p>
                <p>Vui lòng nhấp vào <a href='https://www.thegioididong.com/'>liên kết sau</a> để xác nhận đơn hàng. </p>
                <p>Thank you!</p>";
             }
             
                
                $subjectMail = "XÁC NHẬN ĐƠN HÀNG DH000$idBill";
                $mailcheck = sendMail($email, $name,$contentMail,$subjectMail);
               // echo $mailcheck;
                if($mailcheck){
                    $_SESSION['success_checkout'] = "Đặt hàng thành công, chúng tôi sẽ liên hệ với bạn sau ít phút...";
                    header('Location: thanh-toan.html');
                    unset($_SESSION['cart']);
                }
                else{
                    $_SESSION['error_checkout'] = "Vui lòng thử lại!!!";
                    header('Location: thanh-toan.html');
                }
               // echo  $contentMail;
                //echo $idBill;
            }
        }
    }
}

  /**
 * Cart Object


 (
    [items] => Array
        (
            [105] => Array
                (
                    [qty] => 2
                    [price] => 59800000
                    [promotionPrice] => 58000000
                    [item] => stdClass Object
                        (
                            [id] => 105
                            [name] => iPhone X 64GB Gray
                            [image] => d1OlStyng8-img_2425_copy_master.JPG
                            [price] => 29900000
                            [promotion_price] => 29000000
                        )
                )
            [100] => Array
                (
                    [qty] => 1
                    [price] => 39500000
                    [promotionPrice] => 39500000
                    [item] => stdClass Object
                        (
                            [id] => 100
                            [name] => MPXV2 -Macbook Pro Retina 2017 13 inch 256GB TouchBar ( Gray Space )
                            [image] => mpxv2--macbook-pro-retina-2017-13-inch-256gb-touchbar-(-gray-space-).png
                            [price] => 39500000
                            [promotion_price] => 39500000
                        )
                )
        )
    [totalQty] => 3
    [totalPrice] => 99300000
    [promtPrice] => 97500000
)
 */
?>