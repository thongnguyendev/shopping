<?php
require_once'DBConnect.php';
class IndexModel extends DBConnect{
    
    function getSlide($status=1){
    $sql="SELECT* FROM slide WHERE status=$status";
    
    // có 2 cách return nè .
    return parent::getMoreRow($sql);
    // hoặc có thể sd   return $this->getMoreRow($sql);
    
    }

    // HÀM CHỌN NHỮNG SP DẶC BIỆT
    function selectSpecialProduct()
    {
        // SD PHÉP KẾT BẰNG giữa bảng sản phẩm và pages_url
        
        $sql = "SELECT p.*, u.url AS url
                FROM products p INNER JOIN page_url u  ON p.id_url = u.id
                WHERE status=1 AND deleted=0 
                order by id desc
                LIMIT 0,15";
        return $this->getMoreRow($sql);
    }
    // những sản phẩm bán chạy nhất
    function selectBestProducts()
    {
        // kết 3 bảng và lấy dk chung là id và url ... vì tong so luong ban chạy thì quantity cang lon nên sắp xếp giảm
        
        $sql = "SELECT p.*, sum(bd.quantity) AS tong_soluong, u.url 
                FROM products p
                INNER JOIN bill_detail bd
                ON p.id = bd.id_product
                INNER JOIN page_url u
                ON p.id_url = u.id
                AND deleted=0
                GROUP BY p.id
                ORDER BY tong_soluong DESC  
                LIMIT 0,10";
        return $this->getMoreRow($sql);
    }
    // nhung san pham moi nhat
    
    function newProducts(){
        $sql= "SELECT p.*,u.url
         FROM products p INNER JOIN page_url u  ON p.id_url = u.id
        where new=1 and deleted=0
        limit 0,3";
        return $this->getMoreRow($sql);
    }
    
// những sản phẩm giảm giá

function products_Promotion(){
    
    // sd phép kết giữa các bảng lại

    $sql= " SELECT p.*,u.url
   FROM products p INNER JOIN page_url u  ON p.id_url = u.id
   WHERE promotion_price !=0
   LIMIT 1,3";
        return $this->getMoreRow($sql);
}
    
}
?>